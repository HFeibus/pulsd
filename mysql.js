var mysql = require('mysql');

var mysqlPool = mysql.createPool({
    host: 'rds-mysql-10mintutorial.cqq0wjmszybw.us-east-2.rds.amazonaws.com',
    port: 3306,
    user: 'masterUsername',
    password: 'mypassword',
    database: 'dbname',
});

module.exports = query;
module.exports.end = end;

function query() {
    return mysqlPool.query.apply(mysqlPool, arguments);
}

function end() {
    mysqlPool.end();
}