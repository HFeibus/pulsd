var express = require('express');
var router = express.Router();
var data = require('../data.js');

/* GET home page. */
router.get('/', function(req, res, next) {
  data.getRequirements(function (err, results) {
  	if (err) {
  		return next(err);
  	}
  	res.render('admin', { title: 'Admin', required: results });
  });
});

router.post('/event', function(req, res, next) {
	data.addOrUpdateEvent(req.body, function (err, results) {
		res.send(results);
	});
});

module.exports = router;
