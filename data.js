var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mysql = require('./mysql');
var eventbrite = require('eventbrite');

module.exports.addOrUpdateEvent = addOrUpdateEvent;
module.exports.getRequirements = getRequirements;

//const eb_client = eventbrite({ 'api_key': , 'user_key':  })

function getRequirements ( callback ) {
	mysql({
		sql: 'SELECT * FROM Event_Requirements',
	}, function (err, results) {
		if (err || !results || results.length == 0) {
			return callback(err, {});
		}
		var result = {};
		for (var i = 0; i < results.length; i++) {
			result[results[i].website] = results[i];
		}
		return callback(err, result);
	});
}

function addOrUpdateEvent ( event, callback ) {
	var file = event.imageFile;
	var sites = event.sites;
	delete event.imageFile; delete event.sites;
	//event.image = null;
	mysql({
		sql: 'INSERT INTO Event SET ? ON DUPLICATE KEY UPDATE ?',
		values: [event, event],
	}, function (err, results) {
		// todo: add picture here then

		return addToAllSites(event, sites, callback);
	});
}

function addToAllSites( event, sites, callback ) {
	var count = 0;
	var keys = Object.keys(sites);
	var target = keys.length;
	for (var i = 0; i < keys.length; i++) {
		addToSite(event, keys[i], function (err, results) {
			if (err) {
				return callback(err);
			}
			count++;
			if (count == target) {
				return callback(null, results);
			}
		});
	}
	if (target == 0) {
		return callback(null, 'done');
	}
}

function addToSite ( event, site, callback ) {
	switch (site) {
		case 'eventbrite':
			addToEventbrite(event, callback);
			break;
		default:
			return callback('bad site name');
	}
}

function addToEventbrite ( event, callback ) {

	return callback(null, 'done');
}